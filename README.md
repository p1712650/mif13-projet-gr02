# MIF13 - La Guilde des Trouvailles

<br>

## Quelques Informations
#### URLs des déploiements:

| Nom | Chemin |
| --- | ---    |
| Client Joueur | https://192.168.75.68 |
| Client Admin | https://192.168.75.68/admin-gui |
| API Spring | https://192.168.75.68/auth |
| API Express, partie privee | https://192.168.75.68/admin |
| API Express, partie publique | https://192.168.75.68/api |

#### Comptes utilisateur déjà créés:

| Nickname | Password |
| --- | --- |
| *John* | `JohnP@ssw0rd` |
| *Susan* | `SusanP@ssw0rd` |

<br>

## Serveur Spring
- [Lien vers le fichier yaml](users-api.yaml)
- [Lien vers le Swagger sur la VM](https://192.168.75.68:8443/users/swagger-ui/index.html)

<br>

## Différentes versions des TPs
Les branches à prendre en compte sont les branches : 
- main

Les branches **à ne pas** prendre en compte sont les branches : 
- dev-local
- test-ci 
- pwa

<br>

## Auteurs
- Eldar KASMAMYTOV p1712650
- Melvyn BERTOLONE p1804487 (pseudo : Wilalde)
