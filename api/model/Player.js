import GeoResource from "./GeoResource.js";
import Role from "./Role.js";

export default class Player extends GeoResource {

    static count = 0;

    constructor(name, position, url, ttl, treasures) {
        super(`player-${Player.count++}`, name, position, url, Role.player);
        this.ttl = ttl;
        this.treasures = treasures;
    }

}