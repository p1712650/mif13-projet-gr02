import GeoResource from "./GeoResource.js";

export default class Treasure extends GeoResource {

    static count = 0;

    constructor(name, position, composition, description) {
        super(`treasure-${Treasure.count++}`, name, position);
        this.composition = composition;
        this.description = description;
    }

}