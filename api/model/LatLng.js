export default class LatLng {

    lat = undefined;
    lng = undefined;

    constructor(lat, lng) {
        this.lat = lat;
        this.lng = lng;
    }

    isValid() {
        return this.lat !== undefined &&
            this.lng !== undefined &&
            this.lat !== '' &&
            this.lng !== '';
    }

}