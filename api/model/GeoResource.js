export default class GeoResource {

    constructor(id, name, position, url, role) {
        this.id = id;
        this.name = name;
        this.position = position;
        this.url = url;
        this.role = role;
    }

}