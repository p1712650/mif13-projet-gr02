import express from "express"; // importing the default export of es6 express module
import admin from "./routes/admin.js";
import game from "./routes/game.js";

const app = express();
const port = 3000;

app.use((req, res, next) => {
    console.log('\nTime: ', Date.now());
    next();
});

app.get('/', (req, res) => {
    res.send("Hello World!");
});

app.use('/static', express.static('public'));

app.use('/admin', admin);

app.use('/game', game);

app.use((req, res) => {
    res.status(404).send("Sorry can't find that!");
});

app.listen(port, () => {
    console.log(`Server started at port ${port}`);
});