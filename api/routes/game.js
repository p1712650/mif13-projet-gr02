import axios from "axios";
import * as express from "express";
import cors from "cors";
import https from "https";

import LatLng from "../model/LatLng.js";
import Player from "../model/Player.js";
import Resources from "../model/Resources.js";
import state from "../model/state.js";
import Treasure from "../model/Treasure.js";

const router = express.Router();
router.use(express.json());


function authenticate(jwt, origin) {
    const httpsAgent = new https.Agent({ rejectUnauthorized: false });
    console.log("@authenticate(jwt, origin)");
    return axios.get("http://localhost:8080/users/authenticate", {
        httpsAgent: httpsAgent,
        params: {
            jwt: jwt,
            origin: origin
        }
    });
}

function isInZone(lat, lng) {
    let zoneStart = state.zoneStart;
    let zoneEnd = state.zoneEnd;

    let minLat = zoneStart.lat < zoneEnd.lat ? zoneStart.lat : zoneEnd.lat;
    let minLng = zoneStart.lng < zoneEnd.lng ? zoneStart.lng : zoneEnd.lng;
    let maxLat = zoneStart.lat > zoneEnd.lat ? zoneStart.lat : zoneEnd.lat;
    let maxLng = zoneStart.lng > zoneEnd.lng ? zoneStart.lng : zoneEnd.lng;

    if (lat > minLat && lat < maxLat && lng > minLng && lng < maxLng) {
        return true;
    }

    return false;
}

const distance = function (latlng1, latlng2) {
    const R = 6371000;
    let rad = Math.PI / 180,
        lat1 = latlng1.lat * rad,
        lat2 = latlng2.lat * rad,
        sinDLat = Math.sin((latlng2.lat - latlng1.lat) * rad / 2),
        sinDLon = Math.sin((latlng2.lng - latlng1.lng) * rad / 2),
        a = sinDLat * sinDLat + Math.cos(lat1) * Math.cos(lat2) * sinDLon * sinDLon,
        c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    return R * c;
};

router.get('/resources', (req, res) => {
    console.log("GET /resources");

    // check whether user is alive or not
    // if yes => all existing resources
    // else => only that user's resources

    let jwt = req.get("Authorization");
    let origin = req.get("Origin");

    console.log("jwt: ", jwt);
    console.log("origin: ", origin);

    authenticate(jwt, origin)
    .then(response => {
        const status = response.status;
        console.log("Auth Server response status: ", status);
        console.log("User is authenticated !");

        res.send(Resources);
    })
    .catch(error => {
        const status = error.status;
        console.log("Auth Server response status: ", status);
        console.log(error);

        res.status(401)
           .send("User is not authenticated !");
    });
});

router.put('/resources/:id', (req, res) => {
    console.log("PUT /resources/", req.params.id);
    console.log("ID: ", req.params.id);
    console.log("Params: ", req.params);

    let jwt = req.get("Authorization");
    let origin = req.get("Origin");

    console.log("jwt: ", jwt);
    console.log("origin: ", origin);

    authenticate(jwt, origin)
    .then(response => {
        const status = response.status;
        console.log("Auth Server response status: ", status);
        console.log("User is authenticated !");

        res.send("OK PUT /resources/", req.params.id);
    })
    .catch(error => {
        const status = error.status;
        console.log("Auth Server response status: ", status);
        console.log(error);

        res.status(401)
           .send("User is not authenticated !");
    });
});

router.put('/resources/:id/image', (req, res) => {
    console.log("PUT /resources/" + req.params.id + "/image");
    console.log("ID: ", req.params.id);
    console.log("Params: ", req.params);

    let jwt = req.get("Authorization");
    let origin = req.get("Origin");

    console.log("jwt: ", jwt);
    console.log("origin: ", origin);

    authenticate(jwt, origin)
    .then(response => {
        const status = response.status;
        console.log("Auth Server response status: ", status);
        console.log("User is authenticated !");

        res.send("OK PUT /resources/", req.params.id, "/image");
    })
    .catch(error => {
        const status = error.status;
        console.log("Auth Server response status: ", status);
        console.log(error);

        res.status(401)
           .send("User is not authenticated !");
    });
});

router.options('/player', cors());
router.post('/player', cors(), (req, res) => {
    console.log("POST /player");
    console.log("params", req.params);
    console.log("query", req.query);
    console.log("body", req.body);

    let jwt = req.get("Authorization");
    let origin = req.get("Origin");

    console.log("Authorization: ", jwt);
    console.log("Origin: ", origin);

    authenticate(jwt, origin)
    .then(response => {
        const status = response.status;
        console.log("Auth Server response status: ", status);
        console.log("User is authenticated !");
    
        if (state.gameStatus === "started") {
            const username = req.body.username;
            const position = new LatLng(Number(req.body.position.lat), Number(req.body.position.lng));
            const url = "https://www.santevet.com/upload/admin/images/article/Chien%202/races_de_chiens/siberian-husky.jpg";
            const treasures = [];

            state.player = new Player(username, position, url, state.ttl, treasures);
            console.log(state.player);

            state.ttlDecrementInterval = setInterval(function() {
                if (--state.player.ttl <= 0) clearInterval(state.ttlDecrementInterval);
            }, 1000);

            let responseObj = {
                status: 200,
                message: state.gameStatus,
                ttl: state.ttl,
                zone: {
                    start: {
                        lat: state.zoneStart.lat,
                        lng: state.zoneStart.lng
                    },
                    end: {
                        lat: state.zoneEnd.lat,
                        lng: state.zoneEnd.lng
                    }
                }
            };

            res.send(responseObj);
        }
        else {

            let responseObj = {
                status: 300,
                message: "Game has not been started by the Admin yet"
            };

            res.status(300).send(responseObj);
        }
    })
    .catch(err => {
        const status = err.status;
        console.log("Auth Server response status: ", status);
        console.log(err);

        res.status(401).send({ error: "User is not authenticated !" });
    });
});

router.options('/player', cors());
router.delete('/player', cors(), (req, res) => {
    console.log("DELETE /player");
    console.log("params", req.params);
    console.log("query", req.query);
    console.log("body", req.body);

    let jwt = req.get("Authorization");
    let origin = req.get("Origin");

    // console.log("Authorization: ", jwt);
    // console.log("Origin: ", origin);

    authenticate(jwt, origin)
    .then(() => {
        console.log("User is authenticated !");
    
        if (state.gameStatus === "started" && state.player) {

            setStateGameOver();

            let responseObj = {
                status: 200,
                message: state.gameStatus
            };

            res.send(responseObj);
        }
        else if (state.gameStatus === "over") {

            let responseObj = {
                status: 300,
                message: "No ongoing game"
            };

            res.status(300).send(responseObj);
        }
        else {

            let responseObj = {
                status: 300,
                message: "Game has not been started by the Admin yet"
            };

            res.status(300).send(responseObj);
        }
    })
    .catch(err => {
        console.log(err);

        res.status(401).send({ error: "User is not authenticated !" });
    });
});

router.options('/player/position', cors());
router.put('/player/position', cors(), (req, res) => {
    console.log("PUT /player/position");
    console.log("params", req.params);
    console.log("query", req.query);
    console.log("body", req.body);

    let jwt = req.get("Authorization");
    let origin = req.get("Origin");

    // console.log("Authorization: ", jwt);
    // console.log("Origin: ", origin);

    authenticate(jwt, origin)
    .catch(error => {
        const status = error.status;
        console.log("Auth Server response status: ", status);
        console.log(error);

        res.status(401).send({
            status: 401,
            message: "User is not authenticated !"
        });
    })
    .then(response => {
        const status = response.status;
        console.log("Auth Server response status: ", status);
        console.log("User is authenticated !");

        let lat = Number(req.body.position.lat);
        let lng = Number(req.body.position.lng);

        if (!isInZone(lat, lng)) throw new Error("Outside of the Zone");
        else if (state.player.ttl <= 0) throw new Error("Time is out");
        else {

            Resources.forEach(r => {
                if (r instanceof Treasure) {
                    let meters = distance(r.position, new LatLng(lat, lng));
                    if (meters <= 2) {

                        state.player.treasures.push(r);
                        Resources.splice(Resources.indexOf(r), 1);
                        
                        if (r.name === 'Potion de Lune') {
                            state.player.ttl += 10;
                        }
                        else if (r.name === 'Potion de Dissimulation') {
                            if (state.player.invisible === undefined || state.player.invisible === false) {
                                state.player.invisible = true;
                                clearInterval(state.ttlDecrementInterval);
                            }
                        }
                        else if (r.name === 'Gaz Mortel') {
                            throw new Error("Deadly Gas killed you");
                        }
                    }
                }
            });

            state.player.position.lat = lat;
            state.player.position.lng = lng;

            console.log(state.player);
            console.log(Resources);

            res.send({
                status: 200,
                treasures: Resources,
                treasuresGot: state.player.treasures,
                ttl: state.player.ttl
            });
        }

    })
    .catch(error => {
        console.log("Game is over: ", error.message);

        res.send({
            status: 200,
            gameOver: true,
            message: error.message,
            ttl: state.player.ttl
        });
    });
});

router.options('/status', cors());
router.get('/status', cors(), (req, res) => {
    console.log("GET /status");
    console.log("Params: ", req.params);
    console.log("Body: ", req.body);

    res.send({ started: state.gameStatus === "started" });
});

function setStateGameOver() {
    state.gameStatus = "over";
    state.player = undefined;
    if (state.ttlDecrementInterval) clearInterval(state.ttlDecrementInterval);
    Resources.splice(0, Resources.length);
}

export default router;
