import * as express from "express";
import LatLng from "../model/LatLng.js";
import Resources from "../model/Resources.js";
import Treasure from "../model/Treasure.js";
import state from "../model/state.js";

const router = express.Router();

router.use(express.json());

router.post('/start', (req, res) => {
    console.log('POST /start');
    console.log("headers", req.headers);
    console.log("params", req.params);
    console.log("body", req.body);

    try {
        const zoneStart = new LatLng(Number(req.body.zone[0].lat), Number(req.body.zone[0].lng));
        const zoneEnd = new LatLng(Number(req.body.zone[1].lat), Number(req.body.zone[1].lng));
        const ttl = Number(req.body.ttl === '' ? 0 : req.body.ttl);

        if (zoneStart.isValid() && zoneEnd.isValid() && ttl !== 0) {
            console.log("Data is filled");

            // TODO: need to validate data...

            state.gameStatus = "started";
            state.zoneStart = zoneStart;
            state.zoneEnd = zoneEnd;
            state.ttl = ttl;

            console.log(state);
        }

        res.status(204).send();
    }
    catch (error) {
        // console.error(error);
        res.status(400).send();
    }
});

// router.get('/treasures', (req, res) => {
//     res.send('GET /treasures');
//     console.log("params", req.params);
// });

// router.get('/treasures/:id', (req, res) => {
//     res.send('GET /treasures/', req.params.id);
//     console.log("params", req.params);
// });

router.post('/treasure', (req, res) => {
    console.log("POST /treasure");
    console.log("params", req.params);
    console.log("body", req.body);

    try {
        let pos = new LatLng(req.body.position.lat, req.body.position.lng);
        let name = req.body.type;

        let comp = "This is comp";
        let descr = "This is descr";
        let treasure = new Treasure(name, pos, comp, descr);
        Resources.push(treasure);
        console.log(`Resources: ${Resources}`);

        res.send(treasure);
    }
    catch(err) {
        res.status(400).send();
    }
});

router.get("/player", (req, res) => {
    console.log("GET /player");
    console.log("params", req.params);
    console.log("query", req.query);
    console.log("Body: ", req.body);

    if (state.player !== undefined) {
        let data = {};
        if (req.query.mininfo === 'true') {
            data.position = state.player.position;
            data.ttl = state.player.ttl;
            data.treasures = state.player.treasures;
        }
        else {
            data = state.player;
        }
        console.log(data);
        res.send({
            gameStatus: state.gameStatus,
            player: data
        });
    }
    else {
        let msg = state.gameStatus === "over" ? "Game Over" : "Player is not connected yet";
        res.send({
            gameStatus: state.gameStatus,
            message: msg
        });
    }
});

router.get("/player/position", (req, res) => {
    console.log("GET /player/position");
    console.log("params", req.params);

    if (state.player !== undefined) {
        res.send(state.player.position);
    }
    else {
        res.status(400).send({ error: "Player is not connected yet" });
    }
});

export default router;
