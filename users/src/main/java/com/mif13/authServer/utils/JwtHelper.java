package com.mif13.authServer.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import java.util.Date;
import javax.validation.constraints.NotNull;

/**
 * Classe qui centralise les opérations de validation et de génération d'un token "métier",
 * c'est-à-dire dédié à cette application.
 *
 * @author Lionel Médini
 */
public class JwtHelper {

    private static final String SECRET = "monsecret2022";
    private static final String ISSUER = "TP M1IF13";
    private static final long LIFETIME = 1800000; // Durée de vie d'un token (en ms) : 30 minutes
    private static final Algorithm algorithm = Algorithm.HMAC256(SECRET);
    private static final JWTVerifier adminVerifier =
        JWT.require(algorithm).withClaim("admin", true).build(); // Reusable verifier instance

    /**
     * Vérifie l'authentification d'un utilisateur grâce à un token JWT
     *
     * @param token  le token à vérifier
     * @param origin l'origine de la requête HTTP
     * @return un booléen qui indique si le token est bien formé et valide (pas expiré) et si
     * l'utilisateur est authentifié
     * @throws JWTVerificationException si le token est invalide
     * @throws NullPointerException     si le token n'existe pas
     */
    public static String verifyToken(String token, @NotNull String origin)
        throws NullPointerException, JWTVerificationException {
        JWTVerifier authenticationVerifier = JWT.require(algorithm)
            .withIssuer(ISSUER)
            .withAudience(origin) // Non-reusable verifier instance
            .build();

        authenticationVerifier.verify(token);
        // Pourrait lever une JWTDecodeException mais comme le token est vérifié avant, cela ne devrait pas arriver
        DecodedJWT jwt = JWT.decode(token);

        return jwt.getClaim("sub").asString();
    }

    /**
     * Crée un token avec les caractéristiques de l'utilisateur
     *
     * @param subject le login de l'utilisateur
     * @param origin  l'origine de la requête HTTP
     * @return le token signé
     * @throws JWTCreationException si les paramètres ne permettent pas de créer un token
     */
    public static String generateToken(String subject, String origin)
        throws JWTCreationException {
        return JWT.create()
            .withIssuer(ISSUER)
            .withSubject(subject)
            .withAudience(origin)
            .withExpiresAt(new Date(new Date().getTime() + LIFETIME))
            .sign(algorithm);
    }

}
