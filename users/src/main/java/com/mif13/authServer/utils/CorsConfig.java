package com.mif13.authServer.utils;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class CorsConfig {

    @Bean
    public WebMvcConfigurer configure() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {

                registry.addMapping("/login")
                    .allowedMethods("POST")
                    .allowedHeaders("*", "Authorization")
                    .exposedHeaders("Authorization")
                    .allowedOrigins("http://localhost:8080", "http://localhost:3000",  "http://localhost:80",
                        "http://localhost:4000", "http://192.168.75.68", "https://192.168.75.68");

                registry.addMapping("/logout")
                    .allowedMethods("POST")
                    .allowedHeaders("*", "Authorization")
                    .allowedOrigins("http://localhost:8080", "http://localhost:3000",  "http://localhost:80",
                        "http://localhost:4000", "http://192.168.75.68", "https://192.168.75.68");

                registry.addMapping("/authenticate")
                    .allowedMethods("GET")
                    .allowedHeaders("*")
                    .allowedOrigins("http://localhost:8080", "http://localhost:3000",  "http://localhost:80",
                        "http://localhost:4000", "http://192.168.75.68", "https://192.168.75.68");

                registry.addMapping("/users/{id}")
                    .allowedMethods("GET")
                    .allowedHeaders("*")
                    .allowedOrigins("http://localhost:8080", "http://localhost:3000",  "http://localhost:80",
                        "http://localhost:4000", "http://192.168.75.68", "https://192.168.75.68");

            }
        };
    }

};