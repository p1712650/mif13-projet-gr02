package com.mif13.authServer.dao;

import com.mif13.authServer.exception.UserCreationException;
import com.mif13.authServer.model.User;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import org.springframework.stereotype.Repository;

@Repository
public class UsersDao implements Dao<User> {

    private Map<String, User> users = new HashMap<>();

    public UsersDao() {
        try {
            users.put("John", new User("John", "JohnP@ssw0rd"));
            users.put("Susan", new User("Susan", "SusanP@ssw0rd"));
        } catch (UserCreationException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Optional<User> get(String id) {
        return Optional.ofNullable(users.get(id));
    }

    @Override
    public Set<String> getAll() {
        return users.keySet();
    }

    @Override
    public void save(User user) {
        users.put(user.getLogin(), user);
    }

    @Override
    public void update(User user, String password) {
        users.get(user.getLogin())
            .setPassword(Objects.requireNonNull(password, "Password cannot be null !"));
    }

    @Override
    public void delete(User user) {
        users.remove(user.getLogin());
    }
}
