package com.mif13.authServer.exception;

public class InvalidPasswordException extends UserCreationException {

    public InvalidPasswordException() {
    }

    public InvalidPasswordException(String errorMessage) {
        super(errorMessage);
    }
}