package com.mif13.authServer.exception;

public class InvalidUsernameException extends UserCreationException {

    public InvalidUsernameException() {
    }

    public InvalidUsernameException(String errorMessage) {
        super(errorMessage);
    }
}