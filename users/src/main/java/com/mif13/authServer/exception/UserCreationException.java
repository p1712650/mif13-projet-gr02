package com.mif13.authServer.exception;

public class UserCreationException extends Exception {

    public UserCreationException() {
    }

    public UserCreationException(String errorMessage) {
        super(errorMessage);
    }
}