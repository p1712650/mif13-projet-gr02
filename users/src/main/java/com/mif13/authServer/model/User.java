package com.mif13.authServer.model;

import com.mif13.authServer.exception.InvalidPasswordException;
import com.mif13.authServer.exception.InvalidUsernameException;
import com.mif13.authServer.exception.UserCreationException;
import java.util.regex.PatternSyntaxException;
import javax.naming.AuthenticationException;

public class User {

    private final String login;
    private String password;

    // Permet d'invalider une connexion même si le token est toujours valide
    private boolean connected;

    public User(String login, String password) throws UserCreationException {
        this.login = login;
        this.password = password;
        this.connected = false;

        if (!verifyLogin()) {
            throw new InvalidUsernameException();
        }
        if (!verifyPassword()) {
            throw new InvalidPasswordException();
        }
    }

    public static boolean verifyLogin(String login) throws PatternSyntaxException {
        String regex = "^[a-zA-Z][a-zA-Z0-9._-]{3,20}$";
        return login.matches(regex);
    }

    public static boolean verifyPassword(String password) throws PatternSyntaxException {
        String regex =
            "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,25}$";
        return password.matches(regex);
    }

    public String getLogin() {
        return login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isConnected() {
        return this.connected;
    }

    public void authenticate(String password) throws AuthenticationException {
        if (!password.equals(this.password)) {
            throw new AuthenticationException("Erroneous password");
        }
        this.connected = true;
    }

    public void disconnect() {
        this.connected = false;
    }

    private boolean verifyLogin() throws PatternSyntaxException {
        String regex = "^[a-zA-Z][a-zA-Z0-9._-]{3,20}$";
        return login.matches(regex);
    }

    private boolean verifyPassword() throws PatternSyntaxException {
        String regex =
            "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,25}$";
        return password.matches(regex);
    }
}