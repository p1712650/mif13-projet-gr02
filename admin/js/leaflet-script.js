/* eslint-disable no-undef */
/* eslint-disable indent */
/* eslint-disable no-unused-vars */

// Map sources
const accessToken = 'pk.eyJ1IjoibWFyYnJleCIsImEiOiJja2tpcnA5ZDgwbndkMnVrN2t4MWs4NjA5In0.SyweHd2yUjuEfmnQ_TFiDg';
const mapboxUrl = 'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=' + accessToken;

// BASE Layers (Tile Layers or tiles)
// base means that only one can be visible on the map at a time
const dark = L.tileLayer(mapboxUrl, {
    id: 'mapbox/dark-v10',
    minZoom: 11,
    maxZoom: 22,
});

const satellite = L.tileLayer(mapboxUrl, {
    id: 'mapbox/satellite-v9',
    minZoom: 11,
    maxNativeZoom: 18,
    maxZoom: 22,
});


// Creation of an instance of the main class in Leaflet
// 'mapid' is the ID of a tag HTML in which the map will be shown
let map = L.map('map', {
    center: [45.78192, 4.86567],
    zoom: 18,
    // maxBounds: L.latLngBounds(L.latLng(45.842, 4.721), L.latLng(45.657, 5.011)), // user cannot drag the map outside these points (does not affect zoom)
    attributionControl: false, // to hide a "Leaflet" annotation at the right bottom corner
    layers: satellite // array of Layers which will be added initially
});

const pointIcon = L.divIcon({className: "point-icon"});

const zoneStart = L.marker([45.78233, 4.86490], {
    icon: pointIcon,
    draggable: true,
    autoPan: true,
    autoPanSpeed: 5
})
.bindTooltip("Zone Start Point")
.addTo(map);

const zoneEnd = L.marker([45.78154, 4.86645], {
    icon: pointIcon,
    draggable: true,
    autoPan: true,
    autoPanSpeed: 5
})
.bindTooltip("Zone End Point")
.addTo(map);

const zoneStartLat = document.getElementById("start-lat");
zoneStartLat.value = zoneStart.getLatLng().lat;
const zoneStartLng = document.getElementById("start-lng");
zoneStartLng.value = zoneStart.getLatLng().lng;

const zoneEndLat = document.getElementById("end-lat");
zoneEndLat.value = zoneEnd.getLatLng().lat;
const zoneEndLng = document.getElementById("end-lng");
zoneEndLng.value = zoneEnd.getLatLng().lng;

const ttl = document.getElementById("ttl");
ttl.value = 60;

const zoneBoundsInitial = [zoneStart.getLatLng(), zoneEnd.getLatLng()];
const zone = L.rectangle(zoneBoundsInitial, {color: "#00ffff", weight: 1}).addTo(map);

function updateZoneBounds() {
    zone.setBounds([zoneStart.getLatLng(), zoneEnd.getLatLng()]);
    map.fitBounds(zone.getBounds());
}

zoneStart.on("dragend", event => {
    const marker = event.target;
    const pos = marker.getLatLng();
    zoneStartLat.value = pos.lat;
    zoneStartLng.value = pos.lng;

    updateZoneBounds();
});

zoneEnd.on("dragend", event => {
    const marker = event.target;
    const pos = marker.getLatLng();
    zoneEndLat.value = pos.lat;
    zoneEndLng.value = pos.lng;

    updateZoneBounds();
});

zoneStartLat.addEventListener('change', event => {
    const input = event.target;
    const val = input.value;
    zoneStart.setLatLng([val, zoneStart.getLatLng().lng]);

    updateZoneBounds();
});
zoneStartLng.addEventListener('change', event => {
    const input = event.target;
    const val = input.value;
    zoneStart.setLatLng([zoneStart.getLatLng().lat, val]);

    updateZoneBounds();
});

zoneEndLat.addEventListener('change', event => {
    const input = event.target;
    const val = input.value;
    zoneEnd.setLatLng([val, zoneEnd.getLatLng().lng]);

    updateZoneBounds();
});
zoneEndLng.addEventListener('change', event => {
    const input = event.target;
    const val = input.value;
    zoneEnd.setLatLng([zoneEnd.getLatLng().lat, val]);

    updateZoneBounds();
});

document.getElementById("reset-params-btn").onclick = () => {
    zoneStart.setLatLng(zoneBoundsInitial[0]);
    zoneEnd.setLatLng(zoneBoundsInitial[1]);
    updateZoneBounds();

    zoneStartLat.value = zoneBoundsInitial[0].lat;
    zoneStartLng.value = zoneBoundsInitial[0].lng;

    zoneEndLat.value = zoneBoundsInitial[1].lat;
    zoneEndLng.value = zoneBoundsInitial[1].lng;
};

document.getElementById("start-game-btn").onclick = () => {
    state.gameStatus = "chest";

    document.querySelectorAll("input").forEach(e => {
        e.disabled = true;
    });

    document.querySelectorAll('input[type="button"]').forEach(e => {
        e.style.display = "none";
    });

    setLayoutGameStarted();

    zoneStart.remove();
    zoneEnd.remove();

    map.invalidateSize(true);
    map.fitBounds(zone.getBounds());
};

function setLayoutGameStarted() {

    let layout = window.matchMedia("(max-width: 600px)");

    document.getElementById("params-block").remove();

    document.getElementById("player-block").style.display = "grid";
    document.getElementById("treasures-block").style.display = "grid";
    document.getElementById("log-block").style.display = "grid";

    const page = document.getElementById("page");

    if (layout.matches) {
        page.style.gridTemplateRows = "auto auto auto 250px";
        page.style.gridTemplateAreas = "'player' 'treasures' 'map' 'log'";
    }
    else {
        page.style.gridTemplateRows = "minmax(5rem, 1fr) minmax(min-content, 1fr) minmax(20vh, 2fr)";
        page.style.gridTemplateAreas = "'player map' 'treasures map' 'log map'";
    }
}

const chestIcon = L.divIcon({className: "chest-icon"});

Chest = L.Marker.extend({
    options: { 
        icon: chestIcon
    },
    statics: {
        next_id: 0,
        count: 0
    }
});

Chest.addInitHook(function() {
    this.options.chest_id = Chest.next_id++;
    Chest.count++;
    this.addEventListener('remove', function() {
        Chest.count--;
    });
});

let chestsLayerGroup = L.layerGroup().addTo(map);
let playerTreasures = [];

map.on('click', event => {
    if (state.gameStatus === "chest" || state.gameStatus === "started") {

        if (zone.getBounds().contains(event.latlng)) {
            L.popup().setLatLng(event.latlng)
                .setContent(`
                <div class="treasure-picker">
                    <button faicon="moon" onclick="createChest(${event.latlng.lat}, ${event.latlng.lng}, 'Potion de Lune')"><i class="fa-solid fa-moon"></i></button>
                    <button faicon="ghost" onclick="createChest(${event.latlng.lat}, ${event.latlng.lng}, 'Potion de Dissimulation')"><i class="fa-solid fa-ghost"></i></button>
                    <button faicon="skull" onclick="createChest(${event.latlng.lat}, ${event.latlng.lng}, 'Gaz Mortel')"><i class="fa-solid fa-skull"></i></button>
                    <button faicon="bitcoin" onclick="createChest(${event.latlng.lat}, ${event.latlng.lng}, 'BitCoin')"><i class="fa-brands fa-bitcoin"></i></button>
                </div>`)
                .openOn(map);
        }
        else {
            L.popup().setLatLng(event.latlng)
                .setContent(`<span style="color: crimson;">Outside of the Zone !</span>`)
                .openOn(map);
        }
    }
});

const baseMaps = {
    'Dark': dark,
    'Satellite': satellite
};

function drawPlayer(lat, lng) {
    console.log(`@drawPlayer`);
    if (state.player === undefined) {
        const playerIcon = L.divIcon({className: "player-icon"});
        state.player = L.marker([lat, lng], {
            icon: playerIcon
        })
        .bindTooltip("Player")
        .addTo(map);
    }
    else {
        console.log(`lat: ${lat}`);
        console.log(`lng: ${lng}`);
        state.player.setLatLng(L.latLng(lat, lng));
    }
}

function removeChest(treasureId) {
    chestsLayerGroup.eachLayer(c => {
        if (c.options.treasure_id === treasureId) {
            chestsLayerGroup.removeLayer(c);
        }
    });
}

L.control.layers(baseMaps).addTo(map);
