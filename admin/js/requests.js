/* eslint-disable no-undef */
/* eslint-disable indent */
/* eslint-disable no-unused-vars */

function filterHttpResponse(response) {
    return new Promise((resolve, reject) => {
        if (response.status >= 400 && response.status < 600) {
            reject(response.status);
        }
        else {
            resolve(response);
        }
    })
    .catch(err => console.error(`Error on http response filtering: ${err}`));
}

function filterHttpResponseAsJson(response) {
    return response
        .json()
        .then(data => {
            if (response.status >= 400 && response.status < 600) {
                throw new Error(`${data.message}`);
            }
            return data;
        })
        .catch(err => console.error(`Error on json response: ${err}`));
}

function createChest(lat, lng, treasureType) {
    console.log(`@createChest(${lat}, ${lng}, ${treasureType})`);

    let p = postTreasure(lat, lng, treasureType)
        .then(treasureResponse => {
            // if POST Treasure OK
            // -> create a Chest in Leaflet
            console.log('< POST /admin/treasure', treasureResponse);

            let chest = new Chest([lat, lng], {
                treasure_id: treasureResponse.id,
                treasure_type: treasureType
            })
            .addTo(map)
            .bindTooltip(getChestContent);

            chestsLayerGroup.addLayer(chest);

            chest.on('click', function() {
                console.log(`Removing chest ${this.options.chest_id}`);
                log(`Removed chest #${this.options.chest_id}`);
                this.remove();
            });

            console.log("chest_id", chest.options.chest_id);
            console.log("Chest.count", Chest.count);

            map.closePopup();

            // addTreasure(treasureType);
            log(`Created chest #${chest.options.chest_id}`);

            return chest;
        })
        .catch(err => console.error(`Error on chest creation: ${err}`));
    
    // If 1st Chest created
    // -> start the game, fetch
    if (state.gameStatus === "chest") {
        state.gameStatus = "started";

        state.intervalGetPlayer = window.setInterval(() => getPlayer(), 5000);

        p.then(chest => {
            let url = `${state.apiPath}/admin/start`;

            let bodyObj = {
                zone: [
                    {
                        lat: zone.getBounds().getNorthWest().lat,
                        lng: zone.getBounds().getNorthWest().lng
                    },
                    {
                        lat: zone.getBounds().getSouthEast().lat,
                        lng: zone.getBounds().getSouthEast().lng
                    }
                ],
                ttl: ttl.value
            };

            let configObj = {
                method: 'POST',
                headers: state.headers,
                body: JSON.stringify(bodyObj)
            };
        
            return fetch(url, configObj)
                .then(filterHttpResponse)
                .then(response => {
                    console.log(`< POST /admin/start`, response);
                    log("Game started !");
                });
        });
    }
    
}

function postTreasure(lat, lng, treasureType) {
    console.log(`@postTreasure(${lat}, ${lng}, ${treasureType})`);

    let url = `${state.apiPath}/admin/treasure`;

    let bodyObj = {
        position: {
            lat: lat,
            lng: lng
        },
        type: treasureType
    };

    let configObj = {
        method: 'POST',
        headers: state.headers,
        body: JSON.stringify(bodyObj)
    };
  
    return fetch(url, configObj)
        .then(filterHttpResponseAsJson);
}

function getPlayer() {
    console.log("@getPlayer()");

    let url = `${state.apiPath}/admin/player`;

    let paramsObj = { mininfo: state.playerConnected };

    let params = new URLSearchParams(paramsObj).toString();
    url += '?' + params;

    let configObj = {
        method: 'GET',
        headers: new Headers().set("Accept", "application/json")
    };
  
    return fetch(url, configObj)
        .then(filterHttpResponseAsJson)
        .then(data => {
            // Update GUI
            console.log(data);

            if (data.player) {
                // if player is currently connected
                let player = data.player;

                if (!state.playerConnected) {
                    // WHEN PLAYER CONNECTED
                    displayPlayer(player.url, player.name, player.ttl);
                    updatePlayerConnectedIndicator(true);
                    state.playerName = player.name;
                    state.playerConnected = true;
                    log(`<span style="font-weight: bold;">${state.playerName}</span> <span style="color: lime; font-weight: bold;">has connected</span>`);
                }
                
                updatePlayerTtl(player.ttl);
                
                // loop through only newly taken treasures
                let diff = player.treasures.filter(x => !playerTreasures.some(y => x.id === y.id));
                diff.forEach(t => {
                    log(`<span style="font-weight: bold;">${state.playerName}</span> got <span style="color: orange; font-weight: bold;">${t.name}</span>`);
                    playerTreasures.push(t);
                    addTreasure(t.name);
                    removeChest(t.id);
                });
                
                drawPlayer(player.position.lat, player.position.lng);
            }
            else {

                if (data.gameStatus === "over") {
                    updatePlayerConnectedIndicator(false);
                    clearInterval(state.intervalGetPlayer);
                    showRestartBtn(true);
                    gameOver();
                }

            }
        });
}

function getPlayerTtl() {
    console.log("@getPlayerTtl()");

    let url = `${state.apiPath}/admin/player/ttl`;

    let configObj = {
        method: 'GET',
        headers: new Headers().set("Accept", "*/*")
    };

    return fetch(url, configObj)
        .then(filterHttpResponseAsJson)
        .then(data => {
            // Update GUI
            console.log(data);
            updatePlayerTtl(data.ttl);
        });
}

function gameOver() {
    console.log("Game is over");
}
