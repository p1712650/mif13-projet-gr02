/* eslint-disable no-undef */
/* eslint-disable indent */
/* eslint-disable no-unused-vars */

window.onload = function() {
    showRestartBtn(false);
};

function getChestContent(chest) {
    return `<span style="font-weight: bold; font-seize: 20px;">${chest.options.treasure_type}</span>`;
}

function showRestartBtn(show) {

    const btn = document.getElementById("start-new-game-btn");
    if (show) {

        if (btn === null) {
            // IF BTN DOES NOT EXIST => ADD
            const block = document.getElementById("title-wrapper");
            const node = document.createElement("input");
            node.setAttribute("type", "button");
            node.setAttribute("value", "New Game");
            node.id = "start-new-game-btn";
            node.onclick = () => location.reload(true);
            block.appendChild(node);
        }
    }
    else {

        if (btn) {
            // IF BTN EXISTS => REMOVE
            btn.remove();
        }
    }
}

function displayPlayer(avatarImg, nickname, ttl) {
    const nicknameTag = document.getElementById("user-name");
    if (nicknameTag === null) {
        // player is not connected -> remove placeholder
        const block = document.querySelector("#player-block .block-content");
        block.innerHTML = `<div id="user-avatar" style="background-image: url(${avatarImg});"></div>
        <div id="user-name"><span>${nickname}</span></div>
        <div id="user-ttl"><span>${ttl}</span></div>`;
    }
}

function updatePlayerConnectedIndicator(isConnected) {
    const indicator = document.querySelector("#player-block .player-connected-indicator");
    if (isConnected) {
        indicator.classList.remove('red');
        indicator.classList.add('green');
    }
    else {
        indicator.classList.remove('green');
        indicator.classList.add('red');
    }
}

function updatePlayerTtl(ttl) {
    const ttlBlock = document.querySelector("#player-block .block-content #user-ttl");
    ttlBlock.innerHTML = `${ttl}`;
}

function log(msg) {
    const block = document.querySelector("#log-block .block-content");

    // check if log block is empty or not
    const msgs = document.querySelectorAll("#log-block .block-content .log-line");
    if (msgs.length === 0) {
        // empty log block -> remove placeholder
        block.innerHTML = "";
    }

    const date = new Date();
    const hours = String(date.getHours()).padStart(2, '0');
    const minutes = String(date.getMinutes()).padStart(2, '0');
    const time = `${hours}:${minutes}`;

    const line = buildLogLine(msg, time);
    block.prepend(line);
}

function buildLogLine(msg, time) {
    const tag = document.createElement("span");
    tag.className = "log-line";
    tag.innerHTML = `<span class="log-msg">${msg}</span><span class="log-msg-time">${time}</span>`;
    return tag;
}

function addTreasure(t) {
    const nodes = document.querySelectorAll("#treasures-block .block-content .treasure");
    
    let exists = false;
    if (nodes.length !== 0) {
        // there are existing treasures in block -> search through
        nodes.forEach(e => {
            if (e.firstElementChild.getAttribute("treasure") === t) {
                // t already present -> just increment
                exists = true;
                const count = parseInt(e.lastChild.innerHTML);
                e.lastChild.innerHTML = `${count + 1}`;
            }
        });
    }
    else {
        // treasures block is empty -> remove placeholder
        document.querySelector("#treasures-block .block-content").innerHTML = "";
    }

    if (!exists) {
        // t is not present -> insert new treasure tag
        const tag = document.createElement("div");
        tag.className = "treasure";

        let icon;
        if (t === "Potion de Lune") {
            icon = '<i class="fa-solid fa-moon"></i>';
        }
        else if (t === "Potion de Dissimulation") {
            icon = '<i class="fa-solid fa-ghost"></i>';
        }
        else if (t === "Gaz Mortel") {
            icon = '<i class="fa-solid fa-skull"></i>';
        }
        else if (t === "BitCoin") {
            icon = '<i class="fa-brands fa-bitcoin"></i>';
        }

        tag.innerHTML = `<span class="treasure-name" treasure="${t}">${icon} ${t}</span><span class="treasure-count">1</span>`;

        const block = document.querySelector("#treasures-block .block-content");
        block.appendChild(tag);
    }
}