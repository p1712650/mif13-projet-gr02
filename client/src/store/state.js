import { defineStore } from "pinia";

const useStateStore = defineStore("state", {

  state: () => {
    const state = {
        appName: "La Guilde des Trouvailles",
        authServerBaseUrl: "https://192.168.75.68/auth", //  http://localhost:8080
        gameServerBaseUrl: "https://192.168.75.68/api", // https://192.168.75.68/api http://localhost:3000/api
        headers: new Headers()
    };

    state.headers.set("Origin", "http://localhost");
    state.headers.set("Accept", "*/*");

    return state;
  },

  actions: {
    async getStatus() {
      console.log("@getStatus()");

      let url = `${this.gameServerBaseUrl}/status`;

      let configObj = {
        method: 'GET',
        headers: new Headers().set("Accept", "application/json")
      };

      return fetch(url, configObj)
        .then(filterHttpResponseAsJson)
        .then(status => {
            console.log(status);
            if (status.started === "true") {
              return;
            }
            else {
              throw new Error("Game has not been started by the admin yet");
            }
        });
    }
  }

});

function filterHttpResponseAsJson(response) {
  return response
    .json()
    .then(data => {
      if (response.status >= 400 && response.status < 600) {
        throw new Error(`${data.name}: ${data.message}`);
      }
      return data;
    })
    .catch(err => console.error(`Error on json response: ${err}`));
}

export default useStateStore;