import { defineStore } from "pinia";
import useStateStore from "@/store/state";

const useUserStore = defineStore("user", {
  state: () => {
    return {
      username: "",
      password: "",
      jwt: "",
      loginTimeInMs: undefined,
      isAuthenticated: false,
      isConnected: false,
      ttl: undefined,
      pos: undefined,
      zone: undefined,
      allTreasures: [],
      myTreasures: [],
      putPositionInterval: undefined,
      watchPosition: undefined
    };
  },
  actions: {
    async login() {
      console.log(`@userStore.login()`);

      const state = useStateStore();

      let url = `${state.authServerBaseUrl}/login`;

      let bodyObj = {
        login: this.username,
        password: this.password
      };

      let configObj = {
        method: "POST",
        headers: state.headers,
        body: new URLSearchParams(bodyObj)
      };

      return fetch(url, configObj)
        .then(response => {
          console.log(`status: ${response.status}`);

          if (response.status === 204) {
            console.log("Login OK");
            const token = response.headers.get("Authorization");
            this.jwt = token;
            this.isAuthenticated = true;
            this.loginTimeInMs = Date.now();
            return response;
          }
          else {
            throw new Error(`[${response.status}] Failed to login`);
          }
        });
    },

    async authenticate() {
      console.log(`@userStore.authenticate()`);

      const state = useStateStore();

      let url = `${state.authServerBaseUrl}/authenticate`;

      let paramsObj = {
        jwt: this.jwt,
        origin: "https://192.168.75.68" //https://192.168.75.68 http://localhost:3000
      };

      let params = new URLSearchParams(paramsObj).toString();
      url += '?' + params;

      let configObj = {
        method: "GET",
        headers: state.headers
      };

      return fetch(url, configObj)
        .then(response => {
          console.log(`status: ${response.status}`);

          if (response.status === 204) {
            console.log("Auth OK");
            this.isAuthenticated = true;
            return response;
          }
          else {
            console.error("Auth FAILED");
            this.isAuthenticated = false;
            throw new Error(`[${response.status}] Failed to authenticate`);
          }
        });
    },

    async logout() {
      console.log(`@userStore.logout()`);

      const state = useStateStore();

      let url = `${state.authServerBaseUrl}/logout`;

      let bodyObj = {
        login: this.username
      };

      let configObj = {
        method: "POST",
        headers: state.headers,
        body: new URLSearchParams(bodyObj)
      };

      return fetch(url, configObj)
        .then(response => {
          console.log(`status: ${response.status}`);

          if (response.status === 204) {
            console.log("Logout OK");
            this.isAuthenticated = false;
            this.username = "";
            this.password = "";
            this.jwt = "";
            return response;
          }
          else {
            throw new Error(`[${response.status}] Failed to logout`);
          }
        });
    },

    updatePos() {
      // mise a jour de la position GPS
      if (navigator.geolocation) {
        this.watchPosition = navigator.geolocation.watchPosition(pos => {
          this.pos = {
            lat: pos.coords.latitude,
            lng: pos.coords.longitude
          };
        }, () => console.error("could not retrieve geolocation"), {
          enableHighAccuracy: true
        });
      }
      else {
        if (!this.pos) {
          this.pos = {
            lat: 45.78202,
            lng: 4.86569
          };
        }
        this.pos = {
          lat: this.pos.lat + 0.00001,
          lng: this.pos.lng + 0.00001
        };
      }
      return this.pos;
    },

    async connect() {
      console.log(`@userStore.connect()`);

      const state = useStateStore();

      let url = `${state.gameServerBaseUrl}/player`;

      let bodyObj = {
        username: this.username,
        position: this.watchPosition ? this.pos : this.updatePos(),
        jwt: this.jwt
      };

      let headers = new Headers(state.headers);
      headers.set("Content-Type", "application/json");
      headers.set("Authorization", this.jwt);

      let configObj = {
        method: "POST",
        headers: headers,
        body: JSON.stringify(bodyObj)
      };

      return fetch(url, configObj)
        .then(filterHttpResponseAsJson)
        .then(response => {
          console.log(`status: ${response.status}`);

          if (response.status === 200) {
            console.log("Connect OK");
            this.zone = response.zone;
            this.ttl = response.ttl;
            this.isConnected = true;
            return response;
          }
          else {
            throw new Error(`[${response.status}] Failed to connect: ${response.message}`);
          }
        });
    },

    async disconnect() {
      console.log(`@userStore.disconnect()`);

      const state = useStateStore();

      let url = `${state.gameServerBaseUrl}/player`;

      let headers = new Headers(state.headers);
      headers.set("Authorization", this.jwt);

      let configObj = {
        method: "DELETE",
        headers: headers
      };

      return fetch(url, configObj)
        .then(filterHttpResponseAsJson)
        .then(response => {
          console.log(`status: ${response.status}`);

          if (response.status === 200) {
            console.log("Disconnect OK");
            this.zone = undefined;
            this.ttl = undefined;
            this.isConnected = false;
            this.allTreasures = [];
            this.myTreasures = [];
            this.pos = undefined;
            if (this.putPositionInterval) clearInterval(this.putPositionInterval);
            if (this.updatePlayerPosInterval) clearInterval(this.updatePlayerPosInterval);
            if (this.updatePlayerTTLInterval) clearInterval(this.updatePlayerTTLInterval);
            if (this.watchPosition) navigator.geolocation.clearWatch(this.watchPosition);
            return response;
          }
          else {
            throw new Error(`[${response.status}] Failed to disconnect: ${response.message}`);
          }
        });
    },

    async putPosition() {
      console.log(`@userStore.putPosition()`);

      const state = useStateStore();

      let url = `${state.gameServerBaseUrl}/player/position`;

      let bodyObj = {
        position: this.pos
      };

      console.log(bodyObj);

      let headers = new Headers(state.headers);
      headers.set("Content-Type", "application/json");
      headers.set("Authorization", this.jwt);

      let configObj = {
        method: "PUT",
        headers: headers,
        body: JSON.stringify(bodyObj)
      };

      return fetch(url, configObj)
        .then(filterHttpResponseAsJson)
        .then(data => {
          if (data.gameOver !== undefined && data.gameOver === true) {
            clearInterval(this.putPositionInterval);
            throw new Error(data.message);
          }
          else {
            this.ttl = data.ttl;
            return data;
          }
        });
    }
  }
});

function filterHttpResponseAsJson(response) {
  return response
    .json()
    .then(data => {
      if (response.status >= 400 && response.status < 600) {
        throw new Error(`${data.name}: ${data.message}`);
      }
      return data;
    })
    .catch(err => console.error(`Error on json response: ${err}`));
}

export default useUserStore;