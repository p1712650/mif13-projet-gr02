import { createRouter, createWebHashHistory } from "vue-router";
import LoginView from "@/views/LoginView.vue";
import HomeView from "@/views/HomeView.vue";
import useUserStore from "@/store/user";

const routes = [
  {
    path: "/",
    name: "home",
    component: HomeView
  },
  {
    path: "/login",
    name: "login",
    component: LoginView
  },
  {
    path: "/map",
    name: "map",
    // lazy-loading (load when the route is visited)
    component: () => import("@/views/MapView.vue")
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

router.beforeEach((to, from, next) => {
  console.log("@router.beforeEach");

  const user = useUserStore();

  const timeNow = Date.now();
  const timeSinceLogin = timeNow - user.loginTimeInMs;
  const dateSinceLogin = new Date(timeSinceLogin);
  const h = dateSinceLogin.getHours();
  const m = dateSinceLogin.getMinutes();
  const s = dateSinceLogin.getSeconds();
  console.log(`Time since last login: ${h}h:${m}m:${s}s`)

  if (to.name !== "login" &&
      to.name !== "home") {
    user.authenticate()
      .then(() => {
        // user.connect()
        //   .then(resp => next())
        //   .catch(err => { throw new Error(`Error while connecting: ${err}`) });
        next();
      })
      .catch(() => router.replace({
        name: "login",
        query: {
          redirectPath: to.path
        }
      }));
  }
  else {
    next();
  }

});

export default router;
