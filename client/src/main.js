import { createApp } from "vue";
import App from "@/App.vue";
import router from "@/router";
import store from "@/store";
import './registerServiceWorker'

const app = createApp(App);

app.use(store);
app.use(router);

router.isReady().then(() => {

  app.mount("#app");


});
const cacheName = "cache-permission";
self.addEventListener("install", event => {
  Notification.requestPermission((result) => {
    event.waitUntil(
      caches.open(cacheName).then(cache => {
        return cache.addAll([result]);
      })
    ).catch(err => console.log(err));
  });
})
