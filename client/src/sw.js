import {precacheAndRoute} from 'workbox-precaching';
import {Workbox} from 'workbox-window';

precacheAndRoute(self.__WB_MANIFEST);

if('serviceWorker' in navigator){
    navigator.serviceWorker.register('/sw.js').then(function(registration) {
      registration.addEventListener('updatefound', function() {
        // If updatefound is fired, it means that there's
        // a new service worker being installed.
        var installingWorker = registration.installing;
        console.log('A new service worker is being installed:', installingWorker);
        // You can listen for changes to the installing service worker's
        // state via installingWorker.onstatechange
      });
    })
    .catch(function(error) {
      console.log('Service worker registration failed:', error);
    });
  } else {
    console.log('Service workers are not supported.');
  }

//This is how you can use the network first strategy for files ending with .js
/*  Workbox.routing.registerRoute(
    /.*\.js/,
    Workbox.strategies.networkFirst()
  )

  // Use cache but update cache files in the background ASAP
  Workbox.routing.registerRoute(
    /.*\.css/,
    Workbox.strategies.staleWhileRevalidate({
      cacheName: 'css-cache'
    })
  )
  
  //Cache first, but defining duration and maximum files
  Workbox.routing.registerRoute(
    /.*\.(?:png|jpg|jpeg|svg|gif)/,
    Workbox.strategies.cacheFirst({
      cacheName: 'image-cache',
      plugins: [
        new Workbox.expiration.Plugin({
          maxEntries: 20,
          maxAgeSeconds: 7 * 24 * 60 * 60
        })
      ]
    })
  )
  */